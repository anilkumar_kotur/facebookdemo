//
//  ViewController.m
//  facebookDemo
//
//  Created by Anil on 25/05/15.
//  Copyright (c) 2015 Anil. All rights reserved.
//

#import "ViewController.h"
#import <Accounts/Accounts.h>
#import <Social/Social.h>
@interface ViewController ()

@property ACAccount* facebookAccount;
@property (retain,nonatomic) UIActivityIndicatorView *activityView;

@end

@implementation ViewController

- (IBAction)getInfo:(id)sender {
    
    //START ACTIVITY INDICATOR
    self.activityView = [[UIActivityIndicatorView alloc]
                        initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    self.activityView.center=self.view.center;
    [self.activityView startAnimating];
    [self.view addSubview:self.activityView];
    ACAccountStore *accountStore = [[ACAccountStore alloc] init];
    ACAccountType *facebookType = [accountStore
                                   accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierFacebook];
    NSDictionary *options = @{ACFacebookAppIdKey: @"896841727042823",
                              ACFacebookPermissionsKey: @[@"read_stream"],
                              ACFacebookAudienceKey: ACFacebookAudienceEveryone};
    
    [accountStore requestAccessToAccountsWithType:facebookType
                                          options:options
                                       completion:^(BOOL granted, NSError *error) {
                                           
                                           if (granted) {
                                               NSArray *accounts = [accountStore
                                                                    accountsWithAccountType:facebookType];
                                               
                                               id facebookAccount = [accounts lastObject];
                                               self.facebookAccount = facebookAccount;
                                               [self getData];
                                               //NSLog(@"Done %@", facebookAccount);
                                           }
                                           else
                                           {
                                               
                                               NSLog(@"%@",error.description);
                                               dispatch_async(dispatch_get_main_queue(), ^{
                                                   [[[UIAlertView alloc] initWithTitle:@"Sorry"
                                                                               message:@"Please Configure your Facebook Account"
                                                                              delegate:nil
                                                                     cancelButtonTitle:@"Okey"
                                                                     otherButtonTitles:nil]show];
                                                   [self.activityView stopAnimating];
                                                   
                                               });
                                           }
                                       }];

}


-(void)getData{
     //  NSDictionary *parameters = @{@"message": @"This is a test"};
    NSURL *feedURL = [NSURL URLWithString:@"https://graph.facebook.com/me/feed"];
    
    SLRequest *feedRequest = [SLRequest
                              requestForServiceType:SLServiceTypeFacebook
                              requestMethod:SLRequestMethodGET
                              URL:feedURL
                              parameters:nil];
    //NSLog(@"AnythingHere?");
    
    feedRequest.account = _facebookAccount;
    [feedRequest performRequestWithHandler:^(NSData *responseData,
                                             NSHTTPURLResponse *urlResponse, NSError *error)
     {
         // Handle response
         NSMutableDictionary* json = [NSJSONSerialization JSONObjectWithData:responseData
                                                              options:kNilOptions
                                                                error:&error];
         
        

         NSLog(@" - ---------------------");
        NSLog(@"responseData = %@",json);
         NSLog(@" - ---------------------");
         NSString * message = [[[json objectForKey:@"data"] objectAtIndex:0] objectForKey:@"message"];
         NSLog(@"oooooooooo%@",message);
       
        dispatch_async(dispatch_get_main_queue(), ^{
         tableViewController *secView = [self.storyboard instantiateViewControllerWithIdentifier:@"secView"];
         secView.messageValue = message;
         [self.navigationController pushViewController:secView animated:YES];
            });
     }];
    
    //STOPACTIVITY INDICATOR
    [self.activityView stopAnimating];
    
    
}

-(void) postOnFb{
    NSLog(@"hello we r ab to post on fb");
}


- (void)viewDidLoad {
    [super viewDidLoad];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}

@end
