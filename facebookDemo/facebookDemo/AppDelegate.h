//
//  AppDelegate.h
//  facebookDemo
//
//  Created by Anil on 25/05/15.
//  Copyright (c) 2015 Anil. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

