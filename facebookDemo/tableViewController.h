//
//  tableViewController.h
//  facebookDemo
//
//  Created by Anil on 26/05/15.
//  Copyright (c) 2015 Anil. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PostViewController.h"

@interface tableViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

@property (strong,nonatomic) NSString *messageValue;

@end
