//
//  PostViewController.h
//  facebookDemo
//
//  Created by Anil on 27/05/15.
//  Copyright (c) 2015 Anil. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface PostViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *textField;


- (IBAction)post:(id)sender;

@end
