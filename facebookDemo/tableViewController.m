//
//  tableViewController.m
//  facebookDemo
//
//  Created by Anil on 26/05/15.
//  Copyright (c) 2015 Anil. All rights reserved.
//

#import "tableViewController.h"

@interface tableViewController ()

@end

@implementation tableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                    style:UIBarButtonItemStyleDone target:self action:@selector(loadPostView)];
    self.navigationItem.rightBarButtonItem = rightButton;
    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifer = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifer];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifer];
    }
    
    cell.textLabel.text = self.messageValue;
    return cell;

}

-(void)loadPostView{
    
       PostViewController *postView = [self.storyboard instantiateViewControllerWithIdentifier:@"postView"];
        [self.navigationController pushViewController:postView animated:YES];
}


@end
